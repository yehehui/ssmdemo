package cn.mju.edu.javabean;

public class Echarts {
    private String id;
    private String title;
    private String tooltip;
    private String legend;
    private String toolbox;
    private String grid;
    private String xAxis;
    private String yAxis;
    private String series;
    private float positionX;
    private float positionY;
    private float height;
    private float width;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTooltip() {
        return tooltip;
    }

    public void setTooltip(String tooltip) {
        this.tooltip = tooltip;
    }

    public String getLegend() {
        return legend;
    }

    public void setLegend(String legend) {
        this.legend = legend;
    }

    public String getToolbox() {
        return toolbox;
    }

    public void setToolbox(String toolbox) {
        this.toolbox = toolbox;
    }

    public String getGrid() {
        return grid;
    }

    public void setGrid(String grid) {
        this.grid = grid;
    }

    public String getxAxis() {
        return xAxis;
    }

    public void setxAxis(String xAxis) {
        this.xAxis = xAxis;
    }

    public String getyAxis() {
        return yAxis;
    }

    public void setyAxis(String yAxis) {
        this.yAxis = yAxis;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public float getPositionX() {
        return positionX;
    }

    public void setPositionX(float positionX) {
        this.positionX = positionX;
    }

    public float getPositionY() {
        return positionY;
    }

    public void setPositionY(float positionY) {
        this.positionY = positionY;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    @Override
    public String toString() {
        return "Echarts{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", tooltip='" + tooltip + '\'' +
                ", legend='" + legend + '\'' +
                ", toolbox='" + toolbox + '\'' +
                ", grid='" + grid + '\'' +
                ", xAxis='" + xAxis + '\'' +
                ", yAxis='" + yAxis + '\'' +
                ", series='" + series + '\'' +
                ", positionX=" + positionX +
                ", positionY=" + positionY +
                ", height=" + height +
                ", width=" + width +
                '}';
    }
}
