<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script src="${pageContext.request.contextPath}/resources/js/echarts.min.js"></script>
</head>
<body>
<button id="saveecharts" type="button" class="btn btn-primary ">保存</button>
<div id="backdround" style="width: 100%;height: 100%;position: relative">
    <c:forEach items="${echartsList}" var="echarts" >
        <div class="echarts_item" id="${echarts.id}" style="position:absolute;width: ${echarts.width}px;height: ${echarts.height}px;margin-top: ${echarts.positionY}px;margin-left: ${echarts.positionX}px"></div>
        <script type="text/javascript">
            var myecharts = echarts.init(document.getElementById("${echarts.id}"))
            var  option= {
                title:
                    <c:if test="${echarts.title.startsWith(\"{\")}">
                    [
                        ${echarts.title}
                    ]
            </c:if>
            <c:if test="${!echarts.title.startsWith(\"{\")}">
            {
                ${echarts.title}
            }
            </c:if>
            ,
            tooltip :
                <c:if test="${echarts.tooltip.startsWith(\"{\")}">
                [
                    ${echarts.tooltip}
                ]
            </c:if>
            <c:if test="${!echarts.tooltip.startsWith(\"{\")}">
            {
                ${echarts.tooltip}
            }
            </c:if>
            ,
            legend: <c:if test="${echarts.legend.startsWith(\"{\")}">
                [
                    ${echarts.legend}
                ]
            </c:if>
            <c:if test="${!echarts.legend.startsWith(\"{\")}">
            {
                ${echarts.legend}
            }
            </c:if>
            ,
            toolbox: <c:if test="${echarts.toolbox.startsWith(\"{\")}">
                [
                    ${echarts.toolbox}
                ]
            </c:if>
            <c:if test="${!echarts.toolbox.startsWith(\"{\")}">
            {
                ${echarts.toolbox}
            }
            </c:if>
            ,
            grid: <c:if test="${echarts.grid.startsWith(\"{\")}">
                [
                    ${echarts.grid}
                ]
            </c:if>
            <c:if test="${!echarts.grid.startsWith(\"{\")}">
            {
                ${echarts.grid}
            }
            </c:if>
            ,
            xAxis : <c:if test="${echarts.xAxis.startsWith(\"{\")}">
                [
                    ${echarts.xAxis}
                ]
            </c:if>
            <c:if test="${!echarts.xAxis.startsWith(\"{\")}">
            {
                ${echarts.xAxis}
            }
            </c:if>
            ,
            yAxis:
                <c:if test="${echarts.yAxis.startsWith(\"{\")}">
                [
                    ${echarts.yAxis}
                ]
            </c:if>
            <c:if test="${!echarts.yAxis.startsWith(\"{\")}">
            {
                ${echarts.yAxis}
            }
            </c:if>
            ,
            series:
                <c:if test="${echarts.series.startsWith(\"{\")}">
                [
                    ${echarts.series}
                ]
            </c:if>
            <c:if test="${!echarts.series.startsWith(\"{\")}">
            {
                ${echarts.series}
            }
            </c:if>
            }
            ;
            myecharts.setOption(option)
        </script>
    </c:forEach>
</div>
</body>
<script src="${pageContext.request.contextPath}/resources/js/jquery-1.12.4.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/myecharts.js"></script>
</html>